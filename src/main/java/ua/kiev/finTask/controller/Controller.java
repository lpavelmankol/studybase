package ua.kiev.finTask.controller;

import ua.kiev.finTask.implementaitions.ParseDataImpl;
import ua.kiev.finTask.interfaces.*;

import java.util.HashMap;

public class Controller {

    private final static long DB_UPDATE_INTERVAL = 5000000000L;
    private StrategyFabric strategyFabric;
    private ValidateDateLogic validateDateLogic;
    private FileReader fileReader;
    private DBManager dbManager;
    private ResultWriter resultWriter;


    public Controller(FileReader fileReader, DBManager dbManager, StrategyFabric strategyFabric,
                      ValidateDateLogic validateDateLogic, ResultWriter resultWriter){
        this.fileReader = fileReader;
        this.dbManager = dbManager;
        this.strategyFabric = strategyFabric;
        this.validateDateLogic = validateDateLogic;
        this.resultWriter = resultWriter;
    }


    public void processing(){
        HashMap<String, CalculateInstrumentValue> calcObjHashMap = new HashMap<>();
        HashMap<String, Double> multipliers = dbManager.getMultipliers();
        ParseDataImpl parsedData = ParseDataImpl.getInstance();
        String str = fileReader.getNextLine();
        long start = System.nanoTime();
        while (str != null) {
            long curr = System.nanoTime();
            if (curr - start >= DB_UPDATE_INTERVAL) {
                multipliers = dbManager.getMultipliers();
                start = curr;
            }
            parsedData = parsedData.newParseData(str);
            if (validateDateLogic.validate(parsedData.getDateInstrument())) {
                if (!calcObjHashMap.containsKey(parsedData.getInstrumentID())) {
                    calcObjHashMap.put(parsedData.getInstrumentID(), strategyFabric.getCalculateObject(parsedData));
                }
                if (multipliers.containsKey(parsedData.getInstrumentID())) {
                    parsedData.applyMultiplier(multipliers.get(parsedData.getInstrumentID()));
                }
                calcObjHashMap.get(parsedData.getInstrumentID()).calculate(parsedData);
            }
            str = fileReader.getNextLine();
        }
        resultWriter.writeResult(calcObjHashMap);

    }



    /*public boolean validate(){
        return validateDateLogic.validate();
    }*/
}
