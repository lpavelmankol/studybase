package ua.kiev.finTask.interfaces;

import java.util.HashMap;

/**
 * Created by Shade on 11.03.2017.
 */
public interface ResultWriter {

    public void writeResult(HashMap<String, CalculateInstrumentValue> strategyMap);
}
