package ua.kiev.finTask.interfaces;

public interface CalculateInstrumentValue {


    public void calculate(ParsedData parsedData);

    public String getResult();

}
