package ua.kiev.finTask.interfaces;

/**
 * Created by Shade on 10.03.2017.
 */
public interface FileReader {

    public String getNextLine();

}
