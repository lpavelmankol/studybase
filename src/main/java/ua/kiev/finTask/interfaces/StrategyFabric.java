package ua.kiev.finTask.interfaces;

import java.util.HashMap;

public interface StrategyFabric {

    public CalculateInstrumentValue getCalculateObject(ParsedData parsedData);
}
