package ua.kiev.finTask.interfaces;

import java.util.HashMap;

/**
 * Created by Shade on 10.03.2017.
 */
public interface DBManager {

    public double getMultiplierByID(ParsedData parsedData);

    public HashMap<String, Double> getMultipliers();

}
