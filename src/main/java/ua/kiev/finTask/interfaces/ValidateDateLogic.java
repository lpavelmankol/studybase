package ua.kiev.finTask.interfaces;

import java.time.LocalDate;

public interface ValidateDateLogic {

    public boolean validate(LocalDate localDate);


}
