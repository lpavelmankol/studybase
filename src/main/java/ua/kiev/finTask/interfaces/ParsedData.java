package ua.kiev.finTask.interfaces;

import java.time.LocalDate;

public interface ParsedData {

    public String getInstrumentID();

    public LocalDate getDateInstrument();

    public Double getValueInstrument();

    public void applyMultiplier(double mult);
}
