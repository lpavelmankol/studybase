package ua.kiev.finTask.calculators;

import ua.kiev.finTask.interfaces.CalculateInstrumentValue;
import ua.kiev.finTask.interfaces.ParsedData;

/**
 * Created by Shade on 09.03.2017.
 */
public class CalculateMax implements CalculateInstrumentValue {

    private double max;

    @Override
    public void calculate(ParsedData parsedData) {
        if (parsedData.getValueInstrument() > max) {
            max = parsedData.getValueInstrument();
        }
    }

    @Override
    public String getResult() {
        return String.valueOf(max);
    }
}
