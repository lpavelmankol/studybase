package ua.kiev.finTask.calculators;

import ua.kiev.finTask.interfaces.CalculateInstrumentValue;
import ua.kiev.finTask.interfaces.ParsedData;

public class CalculateMean implements CalculateInstrumentValue {

    private double sum;
    private int counter;

    @Override
    public void calculate(ParsedData parsedData) {
        sum += parsedData.getValueInstrument();
        counter++;
    }

    @Override
    public String getResult() {
        return String.valueOf(sum/(double) counter);
    }
}
