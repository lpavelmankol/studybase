package ua.kiev.finTask.calculators;

import ua.kiev.finTask.interfaces.CalculateInstrumentValue;
import ua.kiev.finTask.interfaces.ParsedData;

import java.time.LocalDate;

public class CalculateMeanForPeriod implements CalculateInstrumentValue {

    private LocalDate startDate;
    private LocalDate endDate;
    private double sum;
    private int counter;


    public CalculateMeanForPeriod (LocalDate startDate, LocalDate endDate){
        this.startDate = startDate;
        this.endDate = endDate;
    }

    @Override
    public void calculate(ParsedData parsedData) {
        if ((startDate.isBefore(parsedData.getDateInstrument()) && endDate.isAfter(parsedData.getDateInstrument())) ||
                (startDate.isEqual(parsedData.getDateInstrument()) || endDate.isEqual(parsedData.getDateInstrument()))) {
            sum += parsedData.getValueInstrument();
            counter++;
        }
    }

    @Override
    public String getResult() {
        return String.valueOf(sum/(double)counter);
    }
}
