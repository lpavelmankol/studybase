package ua.kiev.finTask.calculators;

import ua.kiev.finTask.interfaces.CalculateInstrumentValue;
import ua.kiev.finTask.interfaces.ParsedData;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

/**
 * Created by Shade on 09.03.2017.
 */
public class CalculateTenNewestSum implements CalculateInstrumentValue {

    private Queue<Double> queue;

    public CalculateTenNewestSum() {
        queue = new LinkedList<>();
    }

    @Override
    public void calculate(ParsedData parsedData) {
        queue.add(parsedData.getValueInstrument());
        if (queue.size() > 10) {
            queue.remove();
        }
    }

    @Override
    public String getResult() {
        double sum = 0;
        while (!queue.isEmpty()) {
            sum += queue.poll();
        }
        return String.valueOf(sum);
    }
}
