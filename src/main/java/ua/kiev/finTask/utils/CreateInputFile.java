package ua.kiev.finTask.utils;

import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Locale;
import java.util.Random;

/**
 * Created by Shade on 15.03.2017.
 */
public class CreateInputFile {

    public static void main(String[] args) {
        String pathToFile = "d:\\input.txt";
        int stringsToAppend = 1000000000;

        StringBuilder sb = new StringBuilder();
        Random r = new Random();
        LocalDate startDate = LocalDate.of(1990, Month.JANUARY, 1);
        LocalDate randomDate;
        long days = ChronoUnit.DAYS.between(startDate, LocalDate.now());
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy", Locale.US);


        try (FileWriter writer = new FileWriter(pathToFile, true)){
            for (int i = 0; i < stringsToAppend; i++) {
                sb.append("INSTRUMENT");
                sb.append(r.nextInt(10000) + 1);
                sb.append(',');
                randomDate = startDate.plusDays(r.nextInt((int) days) + 1);
                sb.append(randomDate.format(dateTimeFormatter));
                sb.append(',');
                sb.append(String.format(Locale.US, "%.9f", r.nextDouble() * 100));
                writer.write(sb.toString());
                writer.append(System.lineSeparator());
                sb.setLength(0);
            }
            writer.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
