package ua.kiev.finTask.utils;

import java.sql.*;

/**
 * Created by Shade on 15.03.2017.
 */
public class CreateDB {

    public static void main(String[] args) {
        String pathToDB = "d:\\finTaskDB";
        int instrumentCount = 10000;
        try {
            Class.forName("org.h2.Driver");
        } catch (ClassNotFoundException e) { e.printStackTrace(); }

        try (Connection conn = DriverManager.getConnection("jdbc:h2:" + pathToDB);
             Statement stmt = conn.createStatement()
        ){
            stmt.execute("CREATE TABLE INSTRUMENT_PRICE_MODIFIER(" +
                    "    ID INT AUTO_INCREMENT PRIMARY KEY, " +
                    "    NAME VARCHAR(255) NOT NULL, " +
                    "    MULTIPLIER REAL DEFAULT 1" +
                    ")");
            PreparedStatement pstmt = conn.prepareStatement("INSERT INTO INSTRUMENT_PRICE_MODIFIER(NAME, MULTIPLIER) VALUES (?,?)");
            for (int i = 1; i < instrumentCount + 1; i++) {
                pstmt.setString(1, "INSTRUMENT" + String.valueOf(i));
                pstmt.setDouble(2, Math.random() + 1);
                pstmt.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
