package ua.kiev.finTask.main;

import ua.kiev.finTask.controller.Controller;
import ua.kiev.finTask.implementaitions.*;

/**
 * Created by BlackAngel on 05.03.2017.
 */
public class Main {

    public static void main(String [] args){
        String inputFilePath = "d:\\input.txt";
        String outputFilePath = "d:\\output.txt";
        String dbPath = "d:\\finTaskDB";

        Controller controller = new Controller(new FileReaderImpl(inputFilePath), new DBManagerImpl(dbPath), new StrategyFabricImpl(),
                new ValidateDateLogicImpl(), new ResultWriterImpl(outputFilePath));
        controller.processing();


    }
}
