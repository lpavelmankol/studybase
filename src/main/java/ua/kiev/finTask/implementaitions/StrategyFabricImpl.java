package ua.kiev.finTask.implementaitions;

import ua.kiev.finTask.calculators.CalculateMax;
import ua.kiev.finTask.calculators.CalculateMean;
import ua.kiev.finTask.calculators.CalculateMeanForPeriod;
import ua.kiev.finTask.calculators.CalculateTenNewestSum;
import ua.kiev.finTask.interfaces.CalculateInstrumentValue;
import ua.kiev.finTask.interfaces.ParsedData;
import ua.kiev.finTask.interfaces.StrategyFabric;

import java.time.LocalDate;
import java.time.Month;
import java.util.HashMap;

public class StrategyFabricImpl implements StrategyFabric {
    @Override
    public CalculateInstrumentValue getCalculateObject(ParsedData parsedData) {

        HashMap<String, CalculateInstrumentValue> strategyMap = new HashMap<>();

        if (parsedData.getInstrumentID().equals("INSTRUMENT1")) {
            return new CalculateMean();
        }
        if (parsedData.getInstrumentID().equals("INSTRUMENT2")) {
            LocalDate startDate = LocalDate.of(2014, Month.NOVEMBER, 1);
            LocalDate endDate = LocalDate.of(2014, Month.NOVEMBER, startDate.lengthOfMonth());
            return new CalculateMeanForPeriod(startDate, endDate);
        }
        if (parsedData.getInstrumentID().equals("INSTRUMENT3")) {
            return new CalculateMax();
        }
        //return new CalculateTenNewestSum();
        return new CalculateMean();

    }
}
