package ua.kiev.finTask.implementaitions;

import ua.kiev.finTask.interfaces.FileReader;

import java.io.*;

/**
 * Created by Shade on 10.03.2017.
 */
public class FileReaderImpl implements FileReader {

    private BufferedReader reader;

    public FileReaderImpl(String path) {
        try {
            reader = new BufferedReader(new InputStreamReader(
                    new FileInputStream(path)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getNextLine() {
        String line = null;
        try {
            line = reader.readLine();
        } catch (IOException e) {e.printStackTrace();}
        return line;
    }

}
