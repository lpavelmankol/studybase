package ua.kiev.finTask.implementaitions;

import ua.kiev.finTask.interfaces.CalculateInstrumentValue;
import ua.kiev.finTask.interfaces.ResultWriter;

import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * Created by Shade on 11.03.2017.
 */
public class ResultWriterImpl implements ResultWriter {

    private String path;

    public ResultWriterImpl(String path) {
        this.path = path;
    }

    @Override
    public void writeResult(HashMap<String, CalculateInstrumentValue> strategyMap) {
        List<String> keys = new ArrayList<>(strategyMap.keySet());
        Collections.sort(keys);
        try (FileWriter writer = new FileWriter(path, false)) {
            for (String key : keys) {
                writer.write(key + " -> " + strategyMap.get(key).getResult());
                writer.append(System.lineSeparator());
            }
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
