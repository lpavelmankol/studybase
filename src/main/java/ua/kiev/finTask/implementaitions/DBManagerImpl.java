package ua.kiev.finTask.implementaitions;

import ua.kiev.finTask.interfaces.DBManager;
import ua.kiev.finTask.interfaces.ParsedData;

import java.sql.*;
import java.util.HashMap;

/**
 * Created by Shade on 10.03.2017.
 */
public class DBManagerImpl implements DBManager {

    private Connection conn;

    public DBManagerImpl(String path) {
        try {
            Class.forName("org.h2.Driver");
        } catch (ClassNotFoundException e) { e.printStackTrace(); }

        try {
            conn = DriverManager.getConnection("jdbc:h2:" + path);
        } catch (SQLException e) { e.printStackTrace();}
    }

    @Override
    public double getMultiplierByID(ParsedData parsedData) {
        double mult = 1;
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT MULTIPLIER FROM INSTRUMENT_PRICE_MODIFIER WHERE NAME='" + parsedData + "'");
            if (rs != null) {
                mult = rs.getDouble("MULTIPLIER");
                rs.close();
            }
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return mult;
    }

    @Override
    public HashMap<String, Double> getMultipliers() {
        HashMap<String, Double> multipliers = new HashMap<>();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM INSTRUMENT_PRICE_MODIFIER");
            if (rs != null) {
                while (rs.next()) {
                    multipliers.put(rs.getString("NAME"), rs.getDouble("MULTIPLIER"));
                }
                rs.close();
            }
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  multipliers;
    }


}
