package ua.kiev.finTask.implementaitions;

import ua.kiev.finTask.interfaces.ValidateDateLogic;

import java.time.DayOfWeek;
import java.time.LocalDate;

/**
 * Created by Shade on 10.03.2017.
 */
public class ValidateDateLogicImpl implements ValidateDateLogic {
    @Override
    public boolean validate(LocalDate localDate) {
        return (localDate.getDayOfWeek() != DayOfWeek.SATURDAY && localDate.getDayOfWeek() != DayOfWeek.SUNDAY);
    }
}
