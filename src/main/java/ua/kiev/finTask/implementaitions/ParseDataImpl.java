package ua.kiev.finTask.implementaitions;

import ua.kiev.finTask.interfaces.ParsedData;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class ParseDataImpl implements ParsedData {
    private final static ParseDataImpl parseData = new ParseDataImpl();
    private String instrumentID;
    private LocalDate dateInstrument;
    private double valueInstrument;
    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy", Locale.US);

    private ParseDataImpl(){
    }

    public static ParseDataImpl getInstance(){
        return parseData;
    }

    public ParseDataImpl newParseData(String instrumentData){
        String[] parsedData = instrumentData.split(",");
        this.instrumentID = parsedData[0];
        this.dateInstrument = LocalDate.parse(parsedData[1], dateTimeFormatter);
        this.valueInstrument = Double.valueOf(parsedData[2]);
        return parseData;
    }


    @Override
    public void applyMultiplier(double mult) {
        valueInstrument *= mult;
    }

    @Override
    public String getInstrumentID() {
        return instrumentID;
    }

    @Override
    public LocalDate getDateInstrument() {
        return dateInstrument;
    }

    @Override
    public Double getValueInstrument() {
        return valueInstrument;
    }


}
