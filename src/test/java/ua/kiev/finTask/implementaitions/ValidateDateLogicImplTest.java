package ua.kiev.finTask.implementaitions;

import org.junit.Test;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Shade on 10.03.2017.
 */
public class ValidateDateLogicImplTest {

    @Test
    public void testValidate() {
        ValidateDateLogicImpl validateDateLogic = new ValidateDateLogicImpl();
        LocalDate date = LocalDate.now().with(TemporalAdjusters.nextOrSame(DayOfWeek.MONDAY));
        assertTrue(validateDateLogic.validate(date));
        date = LocalDate.now().with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY));
        assertFalse(validateDateLogic.validate(date));
    }
}
