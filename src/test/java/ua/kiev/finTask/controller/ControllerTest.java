package ua.kiev.finTask.controller;

import org.junit.Before;
import org.junit.Test;
import ua.kiev.finTask.interfaces.ValidateDateLogic;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;

/**
 * Created by BlackAngel on 05.03.2017.
 */
public class ControllerTest {
    public Controller controller;
    public ValidateDateLogic validateDateLogic;

    @Before
    public void init(){

    }

    @Test
    public void processingTest(){

    }

    @Test
    public void validateTestWorkingDay(){
        LocalDate localDate = LocalDate.now();
        localDate = localDate.with(TemporalAdjusters.next(DayOfWeek.MONDAY));
        //assertTrue(controller.validate(localDate));
    }

    @Test
    public void validateTestWeekendDay(){
        LocalDate localDate = LocalDate.now();
        localDate = localDate.with(TemporalAdjusters.next(DayOfWeek.SUNDAY));
        //assertFalse(controller.validate(localDate));
    }






}
