package ua.kiev.finTask.calculators;

import org.junit.Test;
import ua.kiev.finTask.implementaitions.ParseDataImpl;
import ua.kiev.finTask.interfaces.ParsedData;

import static org.junit.Assert.assertEquals;

/**
 * Created by Shade on 10.03.2017.
 */
public class CalculateTenNewestSumTest {

    @Test
    public void testCalculate(){
        ParseDataImpl parsedData = ParseDataImpl.getInstance();
        parsedData.newParseData("INSTRUMENT1,12-Mar-2015,1");

        CalculateTenNewestSum calc = new CalculateTenNewestSum();
        for (int i = 0; i < 16; i++) {
            calc.calculate(parsedData);
        }
        assertEquals("10.0", calc.getResult());
    }
}
